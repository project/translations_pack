<?php
namespace Drupal\translations_pack\handlers;

use Drupal\content_translation\ContentTranslationHandler as HandlerBase;

class ContentTranslationHandler extends HandlerBase {
  use HandlerTrait;
}
