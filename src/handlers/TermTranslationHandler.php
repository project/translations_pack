<?php
namespace Drupal\translations_pack\handlers;

use Drupal\taxonomy\TermTranslationHandler as HandlerBase;

class TermTranslationHandler extends HandlerBase {
  use HandlerTrait;
}
